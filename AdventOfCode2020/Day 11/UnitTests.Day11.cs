﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using NUnit.Framework;

namespace AdventOfCode2020
{
    [TestFixture]
    public class UnitTestsDay11
    {
        readonly List<string> _inputLines = File.ReadLines("./../../../Day 11/input.txt").ToList();

        [Test]
        public void Part1()
        {
            var map = _inputLines;

            var stabilized = Stabilize(map);

            var count = string.Join("", stabilized).Count(x => x == '#');

            Assert.AreEqual(2406, count);
        }

        [Test]
        public void Part2()
        {
        }

        List<string> Stabilize(List<string> map)
        {
            var last    = new List<string>();
            var current = new List<string>(map);

            while (!current.SequenceEqual(last))
            {
                last    = new List<string>(current);
                current = Resolve(current);
            }

            return current;
        }

        List<string> Resolve(List<string> map)
        {
            var newMap = new List<string>(map);

            for (int y = 0; y < map.Count; y++)
            for (int x = 0; x < map[y].Length; x++)
            {
                char c = map[y][x];

                if (c != 'L' && c != '#')
                    continue;

                if (!MatchAdjacent(map, x, y))
                    continue;


                var sb = new StringBuilder(newMap[y]);
                sb[x] = c == 'L'
                    ? '#'
                    : 'L';

                newMap[y] = sb.ToString();
            }

            return newMap;
        }

        bool MatchAdjacent(List<string> map, int x, int y)
        {
            char c = map[y][x];

            var allAdjacent = new List<char>();

            for (int i = -1; i < 2; i++)
            for (int j = -1; j < 2; j++)
            {
                var yy = y + i;
                var xx = x + j;

                if (xx == x && yy == y)
                    continue;

                if (xx < 0
                 || yy < 0
                 || yy >= map.Count
                 || xx >= map[yy].Length)
                    continue;

                allAdjacent.Add(map[yy][xx]);
            }

            if (c == 'L')
                return allAdjacent.All(z => z != '#');
            else
                return allAdjacent.Count(z => z == '#') >= 4;
        }
    }
}