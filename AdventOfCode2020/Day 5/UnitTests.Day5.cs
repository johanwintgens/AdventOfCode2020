﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using NUnit.Framework;

namespace AdventOfCode2020
{
    [TestFixture]
    public class UnitTestsDay5
    {
        readonly List<string> _inputLines = File.ReadLines("./../../../Day 5/input.txt").ToList();

        [Test]
        public void Part1()
        {
            var seatIds = _inputLines.Select(Seat.Find);

            var highestId = seatIds.Max();

            Assert.AreEqual(866, highestId);
        }

        [Test]
        public void Part2()
        {
            var seatIds = _inputLines.Select(Seat.Find)
                                     .ToList();

            var missingSeatId = Seat.FindMissing(seatIds);

            Assert.AreEqual(583, missingSeatId);
        }
    }
}