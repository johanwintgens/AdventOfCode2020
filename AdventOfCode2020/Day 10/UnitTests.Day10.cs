﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using NUnit.Framework;

namespace AdventOfCode2020
{
    [TestFixture]
    public class UnitTestsDay10
    {
        readonly List<string> _inputLines = File.ReadLines("./../../../Day 10/input.txt").ToList();

        [Test]
        public void Part1()
        {
            var adapters = _inputLines.Select(x => Convert.ToInt32(x)).ToList();

            var chain = ChainAdapters(adapters);
        }

        [Test]
        public void Part2()
        {
        }

        List<int> ChainAdapters(List< int> adapters)
        {
            var chain = new List<int>(adapters);

            chain.Add(0);
            chain.Add(chain.Max() + 3);
            chain.Sort();

            for (int i = 0; i < chain.Count; i++)
            {
                if(i==0 || i == chain.Count-1)
                    continue;

                if (chain[i] - chain[i - 1] > 3
                 || chain[i + 1] - chain[i] > 3)
                {
                    chain.RemoveAt(i);
                    i--;
                }
            }

            return chain;
        }
    }
}