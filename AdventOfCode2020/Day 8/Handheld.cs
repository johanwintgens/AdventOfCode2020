﻿using System;
using System.Collections.Generic;

namespace AdventOfCode2020
{
    public static class Handheld
    {
        static List<int> TestedPointers;
        static Dictionary<int, int> ExecCounter;

        public static int Accumulator { get; private set; }

        public static void ExecuteForced(List<string> instructions)
        {
            TestedPointers = new List<int>();

            var fxd = new List<string>(instructions);

            while (!Execute(fxd))
            {
                fxd = FixInstructions(instructions);
            }
        }

        public static bool Execute(List<string> instructions, int pointer = 0)
        {
            if (pointer == 0)
            {
                Accumulator = 0;
                ExecCounter = new Dictionary<int, int>();
            }

            if (pointer == instructions.Count)
                return true;

            if (ExecCounter.ContainsKey(pointer)) ExecCounter[pointer]++;
            else ExecCounter.Add(pointer, 1);

            if (ExecCounter[pointer] == 2)
                return false;


            var split = instructions[pointer].Split(' ');
            var instr = split[0];
            var num = Convert.ToInt32(split[1]);

            switch (instr)
            {
                case "acc":
                    Accumulator += num;
                    break;

                case "jmp":
                    pointer += num - 1;
                    break;

                default:
                case "nop":
                    break;
            }

            return Execute(instructions, pointer + 1);
        }

        static List<string> FixInstructions(List<string> instructions)
        {
            var fxd = new List<string>(instructions);

            for (int i = 0; i < fxd.Count; i++)
            {
                if (TestedPointers.Contains(i))
                    continue;

                if (fxd[i].StartsWith("nop"))
                {
                    TestedPointers.Add(i);
                    fxd[i] = fxd[i].Replace("nop", "jmp");
                    break;
                }

                if (fxd[i].StartsWith("jmp"))
                {
                    TestedPointers.Add(i);
                    fxd[i] = fxd[i].Replace("jmp", "nop");
                    break;
                }
            }

            return fxd;
        }
    }
}