﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using NUnit.Framework;

namespace AdventOfCode2020
{
    [TestFixture]
    public class UnitTestsDay8
    {
        readonly List<string> _inputLines = File.ReadLines("./../../../Day 8/input.txt").ToList();

        [Test]
        public void Part1()
        {
            List<string> instructions = _inputLines;

            Handheld.Execute(instructions);

            Assert.AreEqual(1654, Handheld.Accumulator);
        }

        [Test]
        public void Part2()
        {
            List<string> instructions = _inputLines;

            Handheld.ExecuteForced(instructions);

            Assert.AreEqual(833, Handheld.Accumulator);
        }
    }
}