﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using AdventOfCode2020.Day_4;
using NUnit.Framework;

namespace AdventOfCode2020
{
    [TestFixture]
    public class UnitTestsDay4
    {
        readonly List<string> _inputLines = File.ReadLines("./../../../Day 4/input.txt").ToList();

        [Test]
        public void CountValidPassports()
        {
            var lines = Input.ToOneLinePerGroup(_inputLines).ToList();

            var passports = lines.Select(Passport.FromLine).ToList();

            var validCount = passports.Count(x => x.IsValid());

            Assert.AreEqual(186, validCount);
        }

        [Test]
        public void ValidTest()
        {
            var testLines = new List<string> {"pid:087499704", "hgt:74in", "ecl:grn", "iyr:2012", "eyr:2030", "byr:1980", "hcl:#623a2f", "", "eyr:2029", "ecl:blu", "cid:129", "byr:1989", "iyr:2014", "pid:896056539", "hcl:#a97842", "hgt:165cm", "", "hcl:#888785", "hgt:164cm", "byr:2001", "iyr:2015", "cid:88", "pid:545766238", "ecl:hzl", "eyr:2022", "", "iyr:2010", "hgt:158cm", "hcl:#b6652a", "ecl:blu", "byr:1944", "eyr:2021", "pid:093154719"};

            var lines = Input.ToOneLinePerGroup(testLines).ToList();

            var passports = lines.Select(Passport.FromLine).ToList();

            var validCount = passports.Count(x => x.IsValid());

            Assert.AreEqual(4, validCount);
        }

        [Test]
        public void InvalidTest()
        {
            var testLines = new List<string> {"eyr:1972", "cid:100", "hcl:#18171d", "ecl:amb", "hgt:170", "pid:186cm", "iyr:2018", "byr:1926", "", "iyr:2019", "hcl:#602927", "eyr:1967", "hgt:170cm", "ecl:grn", "pid:012533040", "byr:1946", "", "hcl:dab227", "iyr:2012", "ecl:brn", "hgt:182cm", "pid:021572410", "eyr:2020", "byr:1992", "cid:277", "", "hgt:59cm", "ecl:zzz", "eyr:2038", "hcl:74454a", "iyr:2023", "pid:3556412378", "byr:2007"};

            var lines = Input.ToOneLinePerGroup(testLines).ToList();

            var passports = lines.Select(Passport.FromLine).ToList();

            var validCount = passports.Count(x => x.IsValid());

            Assert.AreEqual(0, validCount);
        }

        [Test]
        public void PrintValidPassports()
        {
            var lines = Input.ToOneLinePerGroup(_inputLines).ToList();

            var passports = lines.Select(Passport.FromLine).ToList();

            var sb = new StringBuilder();

            foreach (var p in passports.Where(x => x.IsValid()))
                sb.AppendLine($"{p.byr} {p.iyr} {p.eyr} {p.hcl} {p.ecl} {p.pid} {p.hgt}");

            var s = sb.ToString();
        }
    }
}