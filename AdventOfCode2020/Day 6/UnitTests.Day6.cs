﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using NUnit.Framework;

namespace AdventOfCode2020
{
    [TestFixture]
    public class UnitTestsDay6
    {
        readonly List<string> _inputLines = File.ReadLines("./../../../Day 6/input.txt").ToList();

        [Test]
        public void Part1()
        {
            List<string> lines = Input.ToOneLinePerGroup(_inputLines, appendSpace: false);

            List<string> parsed = lines.Select(Duplicates.Remove)
                                       .ToList();

            int sum = parsed.Sum(x => x.Length);

            Assert.AreEqual(6351, sum);
        }

        [Test]
        public void Part2()
        {
            List<string> lines = Input.ToOneLinePerGroup(_inputLines, appendSpace: true);

            List<int> counts = lines.Select(x => x.Split(' '))
                                    .Select(Duplicates.Count)
                                    .ToList();

            int sum = counts.Sum();

            Assert.AreEqual(3143, sum);
        }
    }
}