﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using NUnit.Framework;

namespace AdventOfCode2020
{
    [TestFixture]
    public class UnitTestsDay12
    {
        readonly List<string> _inputLines = File.ReadLines("./../../../Day 12/input.txt").ToList();

        [Test]
        public void Part1()
        {
            var instructions = _inputLines;

            var result = GetPoint(instructions);

            var manDist = GetManhattanDistance(result);

            Assert.AreEqual(1148, manDist);
        }

        [Test]
        public void Part2()
        {
        }

        int GetManhattanDistance(Point p)
        {
            return Math.Abs(p.X) + Math.Abs(p.Y);
        }

        Point GetPoint(List<string> instructions)
        {
            Point p = new Point(0, 0);

            var dir = 'E';
            var directions = new List<char> { 'N', 'E', 'S', 'W' };

            foreach (var instruction in instructions)
            {
                var instr = instruction[0];
                var value = Convert.ToInt32(instruction.Substring(1));

                int i = directions.IndexOf(dir);

                switch (instr)
                {
                    case 'R':
                    {
                        i += value / 90;
                        while (i >= 4)
                            i -= 4;

                        dir = directions[i];
                        continue;
                    }
                    case 'L':
                    {
                        i -= value / 90;
                        while (i < 0)
                            i += 4;

                        dir = directions[i];
                        continue;
                    }
                    case 'F':
                        instr = dir;
                        break;
                }

                switch (instr)
                {
                    case 'N':
                        p.Y += value;
                        break;

                    case 'E':
                        p.X += value;
                        break;

                    case 'S':
                        p.Y -= value;
                        break;

                    case 'W':
                        p.X -= value;
                        break;
                }
            }

            return p;
        }
    }
}