﻿using System.Collections.Generic;
using System.Text;

namespace AdventOfCode2020
{
    public static class Input
    {
        public static List<string> ToOneLinePerGroup(List<string> allLines, bool appendSpace = true)
        {
            int i     = -1;
            var sb    = new StringBuilder();
            var lines = new List<string>();

            while (++i < allLines.Count)
            {
                sb.Append(allLines[i]);

                if (appendSpace)
                    sb.Append(" ");

                if (allLines[i] == "" || i == allLines.Count - 1)
                {
                    lines.Add(sb.ToString().Trim());
                    sb.Clear();
                }
            }

            return lines;
        }
    }
}