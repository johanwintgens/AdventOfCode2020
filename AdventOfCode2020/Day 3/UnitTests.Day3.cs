﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using AdventOfCode2020.Day_3;
using NUnit.Framework;

namespace AdventOfCode2020
{
    [TestFixture]
    public class UnitTestsDay3
    {
        readonly List<string> _inputLines = File.ReadLines("./../../../Day 3/input.txt").ToList();

        [Test]
        public void Part1()
        {
            var map = Map.FromLines(_inputLines);

            var treeCount = map.CountTreesOnSlope(3, 1);

            Assert.AreEqual(240, treeCount);
        }

        [Test]
        public void Part2()
        {
            var xSteps = new[] {1, 3, 5, 7, 1};
            var ySteps = new[] {1, 1, 1, 1, 2};

            var map = Map.FromLines(_inputLines);

            long product = 1;
            for (int i = 0; i < 5; i++)
            {
                var trees = map.CountTreesOnSlope(xSteps[i], ySteps[i]);
                product *= trees;
            }

            Assert.AreEqual(2832009600, product);
        }
    }
}