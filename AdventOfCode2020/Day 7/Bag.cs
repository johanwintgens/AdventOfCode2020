﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2020
{
    public static class Bag
    {
        public static List<string> GetAllHolders(string variant, Dictionary<string, List<string>> bagsDic)
        {
            var allHolders = bagsDic.Where(x => x.Value.Any(y => y == variant))
                                    .Select(x => x.Key)
                                    .ToList();
            
            var lastHoldersCount = 0;
            while (lastHoldersCount != allHolders.Count)
            {
                lastHoldersCount = allHolders.Count;
                allHolders.AddRange(bagsDic.Where(x => x.Value.Any(y => allHolders.Contains(y)) && !allHolders.Contains(x.Key))
                                           .Select(x => x.Key)
                                           .ToList());
            }

            return allHolders;
        }

        public static List<string> GetAllHeld(string topBag, Dictionary<string, List<string>> bagsDic)
        {
            var allHeld = new List<string>();

            var contents = bagsDic[topBag];
            foreach (var content in contents.Distinct())
            {
                var occurrence = contents.Count(x => x == content);
                for (int i = 0; i < occurrence; i++)
                {
                    allHeld.Add(content);
                    allHeld.AddRange(GetAllHeld(content, bagsDic));
                }
            }

            return allHeld;
        }

        public static Dictionary<string, List<string>> LoadMany(IEnumerable<string> rules)
        {
            var bagsDic = new Dictionary<string, List<string>>();

            foreach (var rule in rules)
            {
                var splitRule = rule.Split(" bags contain ");

                var variant = splitRule[0];
                var bags    = ParseBags(splitRule[1]).ToList();

                bagsDic.Add(variant, bags);
            }

            return bagsDic;
        }

        static IEnumerable<string> ParseBags(string bagsString)
        {
            if (bagsString.Contains("no other "))
                yield break;

            var bagsRules = bagsString.Replace(" bags", "")
                                      .Replace(" bag", "")
                                      .TrimEnd('.')
                                      .Split(", ");

            foreach (var bagsRule in bagsRules)
            {
                var count = Convert.ToInt32(bagsRule.Split(' ')[0]);
                var bag   = bagsRule.Replace($"{count} ", "");

                for (int i = 0; i < count; i++)
                    yield return bag;
            }
        }
    }
}