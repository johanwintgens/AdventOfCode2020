﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using NUnit.Framework;

namespace AdventOfCode2020
{
    [TestFixture]
    public class UnitTestsDay7
    {
        const    string       variant     = "shiny gold";
        readonly List<string> _inputLines = File.ReadLines("./../../../Day 7/input.txt").ToList();

        [Test]
        public void Part1()
        {
            Dictionary<string, List<string>> bagsDic = Bag.LoadMany(_inputLines);

            List<string> allHolders = Bag.GetAllHolders(variant, bagsDic);

            int sum = allHolders.Count;

            Assert.AreEqual(268, sum);
        }

        [Test]
        public void Part2()
        {
            Dictionary<string, List<string>> bagsDic = Bag.LoadMany(_inputLines);

            List<string> allHeld = Bag.GetAllHeld(variant, bagsDic);

            int sum = allHeld.Count;

            Assert.AreEqual(7867, sum);
        }
    }
}