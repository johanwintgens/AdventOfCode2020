﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using NUnit.Framework;

namespace AdventOfCode2020
{
    [TestFixture]
    public class UnitTestsDay1
    {
        const int Sum = 2020;
        readonly List<string> _inputLines = File.ReadLines("./../../../Day 1/input.txt").ToList();

        [Test]
        public void TestAddends()
        {
            var possibleAddends = _inputLines.Select(x => Convert.ToInt32(x)).ToList();

            var product = Addends.FindAndMultiply(possibleAddends, Sum);

            Assert.AreEqual(193598720, product);
        }
    }
}