﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using NUnit.Framework;

namespace AdventOfCode2020
{
    [TestFixture]
    public class UnitTestsDay2
    {
        readonly List<string> _inputLines = File.ReadLines("./../../../Day 2/input.txt").ToList();

        [Test]
        public void PasswordTest()
        {
            var passwords = _inputLines.Select(Password.FromLine)
                                       .ToList();

            var validPasswordCount = passwords.Count(x => x.IsValid());

            Assert.AreEqual(708, validPasswordCount);
        }
    }
}