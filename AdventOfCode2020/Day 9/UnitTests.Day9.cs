﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using NUnit.Framework;

namespace AdventOfCode2020
{
    [TestFixture]
    public class UnitTestsDay9
    {
        readonly List<string> _inputLines = File.ReadLines("./../../../Day 9/input.txt").ToList();

        [Test]
        public void Part1()
        {
            List<long> content = _inputLines.Select(x => Convert.ToInt64(x)).ToList();

            long value = XMAS.FindInvalid(content);

            Assert.AreEqual(138879426, value);
        }

        [Test]
        public void Part2()
        {
            List<long> content = _inputLines.Select(x => Convert.ToInt64(x)).ToList();

            List<long> set = XMAS.FindContiguousSet(content);

            long value = set.Min() + set.Max();

            Assert.AreEqual(23761694, value);
        }
    }
}