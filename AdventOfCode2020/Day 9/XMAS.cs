﻿using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2020
{
    public class XMAS
    {
        const int PreLength = 25;

        public static List<long> FindContiguousSet(List<long> content)
        {
            List<long> set = null;
            var value = FindInvalid(content);

            for (int i = 0; i < content.Count; i++)
            {
                var j = i;
                set = new List<long>();

                while (true)
                {
                    set.Add(content[j++]);

                    var sum = set.Sum();
                    if (sum == value)
                        return set;

                    if (sum > value)
                        break;
                }
            }

            return set;
        }

        public static long FindInvalid(List<long> content)
        {
            var preamble = content.GetRange(0, PreLength);

            for (var i = PreLength; i < content.Count; i++)
            {
                var value = content[i];

                if (!IsValid(preamble, value))
                    return value;

                preamble.RemoveAt(0);
                preamble.Add(value);
            }

            return 0;
        }

        static bool IsValid(List<long> preamble, long value)
        {
            for (int i = 0; i < preamble.Count; i++)
            for (int j = 0; j < preamble.Count; j++)
            {
                if (i == j)
                    continue;

                if (value == preamble[i] + preamble[j])
                    return true;
            }

            return false;
        }
    }
}